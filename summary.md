# Fuchsia OS – Summary Page

## Introduction

This video has shown how processes in the Fuchsia operating system are implemented. To do this the video first explained that the base kernel on which it is built upon is Little Kernel (LK). It described how Fuchsia built upon LK to provide required and expected features (such as processes) by using LK constructs. 

## Acquiring the Fuchsia Kernel source code
<br>
```
git clone https://fuchsia.googlesource.com/magenta
```

## Relevant files for process implementation

This following file contains the systems calls for processes. The video looked at the function sys_process_create and  sys_process_start where the latter started a process through the object process dispatcher.

<br>

```
/kernel/lib/syscalls/syscalls_magenta.cpp
```
<br>
Files that implement processes in Magenta. Specifically the video looked into the start function in process_dispatcher.cpp to see how processes started up. What was observed was that only user thread is started in this function. 

<br>

```
/kernel/lib/magenta/include/magenta/process_dispatcher.h
/kernel/lib/magenta/process_dispatcher.cpp
```
<br>
File that shows the implementation of threads in magenta using LK threads (constructs). In the video initialize function was looked at; which showed that these threads are implemented through LK threads.

<br>

```
/kernel/lib/magenta/user_thread.cpp
```
<br>
LK thread implementation files.

<br>

```
/kernel/lib/magenta/user_thread.cpp
```
## References:

https://github.com/fuchsia-mirror/magenta/blob/master/docs/getting_started.md<br> 
http://www.tomshardware.com/news/google-fuchsia-new-operating-system,32475.html
