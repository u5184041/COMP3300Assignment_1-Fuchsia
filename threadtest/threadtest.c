#include <pthread.h>
#include <stdio.h>

void *fn(void *args_ptr) {
    int i;
    for(i=0;i<100;++i) printf("%d* \n", i);
    return NULL;
}

int main() {
    pthread_t thread;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_create(&thread, &attr, fn, NULL);
    int i;
    for(i=0;i<100;++i) printf("%d# \n", i);
    return 0;
}
