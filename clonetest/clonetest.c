#include <stdio.h>
#include <unistd.h>
#include <sched.h>

int clonefn() {
    return getpid();
}

int
main (void)
{
    int pid;
    printf("pid: %d\n", getpid());
    printf("ppid: %d\n", getppid());
    clone();
    return 0;
}


