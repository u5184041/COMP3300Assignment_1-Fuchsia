#include <stdio.h>
#include <unistd.h>

int
main (void)
{
    int pid;
    pid = fork();
    if (pid == 0) {
        return 0;
    } else if (pid > 0) {
        printf("Child pid: %d\n", pid);
    } else {
        printf("fork error\n");
    } 
    return 0;
}

